/*
This file is part of Bodacious Printf.

Bodacious Printf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bodacious Printf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bodacious Printf.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _Bodacious_Printf_h_
#define _Bodacious_Printf_h_ 1

#include <string>

class Bodacious {
private:
	Bodacious () {};
	~Bodacious() {};

public:
  /** Append formatted text to a std::string.   
   *
   * This function works much like standard sprintf,
   * except that the result is appended to a std::string.
   *
   * @param dst is the destination std::string that the result
   *              will be appended to.
   * @param format is the format string, just like in printf,
   * @param ... denotes the vararg arguments used in the format string.
   * @returns the number of chars written to dst or -1 in case of error.
   *
   * NOTE: - If -1 is returned, then either something arcane happened
   *         and it has to do with vsnprintf(),
   *         or you used "%ls" and the wchar_t string you supplied was invalid.
   *       - Internally, this function splits the format string
   *         into sections of plain text which are simply appended to dst
   *         and format fields, such as %2i, which are vsnprint()ed
   *         into a buffer and then appended. Therefore, the format strings
   *         should be perfectly compatible with vsnprintf().
   *       - Did you notice the word "append" used five times in this description?
   *         It means you have to do dst.clear() if you want to start
   *         with an empty std::string.
   */
  static long sprintf( std::string &dst, const char *format,... );

private: 
  static bool extractfmt( char *dst, int dstlen, const char *format, int formatlen );
	static const char *skipdigits( const char *fpos );
	static const char *skipchars( const char *fpos, const char *charlist );
	static long fieldlen( const char *fpos );
	static const char *findfield( const char *fpos, int &len );
  static bool appendWide( std::string &dst, const wchar_t *ws );
  static size_t Wcstombs2( char *dst, const wchar_t *ws, size_t dstlen, size_t *wlen = NULL );
};

#endif //_Bodacious_Printf_h_
