/*
This file is part of Bodacious Printf.

Bodacious Printf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bodacious Printf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bodacious Printf.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bodacious_printf.h"

#include <cstring>
#include <cstdarg>

long Bodacious::sprintf( std::string &dst, const char *format,... ) {
  char  fmt[128];
  char  buf[512];

	va_list args;
	va_start(args, format);

	const char *fpos = format;
	const char *specpos;
	long origsize = (long)dst.size();
	int speclen;

	while( fpos[0] != 0 ) {
		speclen = 0;
		specpos = findfield( fpos, speclen );
		if( specpos == NULL ) {
			dst.append( fpos );
			break;
		}
		dst.append( fpos, specpos - fpos );

    if( specpos[1] == 's' ) {
      dst.append( va_arg( args, const char *) );
    } else if(( specpos[1] == 'l' )&&( specpos[2] == 's' )) {
      if( !Bodacious::appendWide( dst, va_arg( args, wchar_t * )) ) {
        return -1;
      }
    } else {
      extractfmt( fmt, sizeof( fmt ), specpos, speclen );

	  //I hope this will do for VisualStudio
#ifdef WIN32
	  if( 0 > _vsnprintf( buf, sizeof( buf ), fmt, args ) ) {
        return -1;
      }
	  //_vsnprintf doesn't advance the va_args.
	  //So, let's do it manually.
      const char *fp = fmt;
	  va_arg( args, int );
	  while( fp[0] ) {
		  if( fp[0] == '*' ) {
			  va_arg( args, int );
		  }
		  ++fp;
	  }
#else
	  if( 0 > vsnprintf( buf, sizeof( buf ), fmt, args ) ) {
        return -1;
      }
#endif //WIN32
      dst.append( buf );
    }

		fpos = specpos + speclen;
	}
	return (long)dst.size() - origsize;
};

size_t Bodacious::Wcstombs2( char *dst, const wchar_t *ws, size_t dstlen, size_t *wlen ) {
#ifdef WIN32
	char buf[8];
#else
	char buf[MB_CUR_MAX];
#endif
	if( wlen ) { *wlen = 0; }

  const wchar_t *orig_ws = ws;
  char *orig_dst = dst;
  char *dst_end = dst + dstlen;
  int charlen = wctomb( buf, ws[0] );

  while( buf[0] != 0 ) {
    if( (dst + charlen) >= dst_end ) {
      break;
    }
    for( int i = 0; i < charlen; ++i ) {
      dst[0] = buf[i];
      ++dst;
    }
    ++ws;
    charlen = wctomb( buf, ws[0] );
  }
  if( wlen ) { *wlen = ws - orig_ws; }
  return dst - orig_dst;
};

bool Bodacious::appendWide( std::string &dst, const wchar_t *ws ) {
  char buf[1024];
  while( ws[0] != (wchar_t)0 ) {
    size_t wlen;
    size_t len = Bodacious::Wcstombs2( buf, ws, sizeof( buf ) - 1, &wlen );
    if( len == (size_t)-1 ) { return false; }
    buf[ len ] = 0;
    dst.append( buf );
    ws += wlen;
  }
  return true;
};

bool Bodacious::extractfmt( char *dst, int dstlen, const char *format, int formatlen ) {
  if( (formatlen + 1) > dstlen ) {
    return false;
  }
  strncpy( dst, format, formatlen );
  dst[ formatlen ] = 0;
  return true;
};

const char *Bodacious::skipdigits( const char *fpos ) {
	while(( fpos[0] >= '0' )&&( fpos[0] <= '9' )) {
		++fpos;
	}
	return fpos;
};

const char *Bodacious::skipchars( const char *fpos, const char *charlist ) {
	if( fpos[0] == 0 ) { return fpos; }
	const char *cptr = strchr( charlist, fpos[0] );
	while(( cptr != NULL )&&( fpos[0] != 0 )) {
		++fpos;
		cptr = strchr( charlist, fpos[0] );
	}
	return fpos;
};

long Bodacious::fieldlen( const char *fpos ) {
	const char *pos = fpos;
	pos = skipchars( pos, "-+ #0" );
	if( pos[0] == 0 ) { return 0;	}
	if( pos[0] == '*' ) {
		++pos;
	} else {
		pos = skipdigits( pos );
	}
	if( pos[0] == 0 ) { return 0;	}
	if( pos[0] == '.' ) {
		++pos;
		if( pos[0] == '*' ) {
			++pos;
		} else {
			pos = skipdigits( pos );
		}
		if( pos[0] == 0 ) { return 0;	}
	}
	pos = skipchars( pos, "hjlztL" );
	if( pos[0] == 0 ) { return 0;	}
	const char *specpos = skipchars( pos, "csdioxXufFeEaAgGnp" );
	if( specpos == pos ) {
		return 0;
	}
	++pos;
	return (long)(pos - fpos);
};

const char *Bodacious::findfield( const char *fpos, int &len ) {
	fpos = strchr( fpos, '%' );
	while(( fpos != NULL )&&( fpos[1] == '%' )) {
		fpos = strchr( fpos + 2, '%' );
	}
	if(( fpos == NULL )||( fpos[1] == 0 )) {
		len = 0;
		return NULL;
	}

	len = fieldlen( fpos + 1 ) + 1;

	return fpos;
};
