/*
This file is part of Bodacious Printf.

Bodacious Printf is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Bodacious Printf is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Bodacious Printf.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../src/bodacious_printf.h"
#include <string>

class Tester
{
public:
  Tester() : errors(0) {};
  ~Tester() {};

  bool CmpStr( const std::string &actual, const char *expected ) {
    if( 0 != actual.compare( expected ) ) {
      printf("Error!\n  Expected: \"%s\"\n  Actual  : \"%s\"\n", expected, actual.c_str() );
      ++errors;
      return false;
    }
    return true;
  };

  void Verdict() {
    if( !errors ) {
      printf("Wow! No errors!\n");
    } else {
      printf("Tragedy!!! There were %i errors.\n", errors );
    }
  };

  int errors;
};

int main()
{
  Tester t;

  std::string s;

  Bodacious::sprintf(s,"Hello, %i, %i, %i!", 1,2,3 );
  t.CmpStr( s, "Hello, 1, 2, 3!" );

  s.clear(); Bodacious::sprintf(s,"Hello, %s, %i, %i, %i!", "world", 1,2,3 );
  t.CmpStr( s, "Hello, world, 1, 2, 3!" );

  s.clear(); Bodacious::sprintf(s,"Hello, %s (%+05i, %lf)!", "world", -25, 5.5 );
  t.CmpStr( s, "Hello, world (-0025, 5.500000)!" );

  s.clear(); Bodacious::sprintf(s,"%x", 322424845 );
  t.CmpStr( s, "1337d00d" );

  s.clear(); Bodacious::sprintf(s,"%05.2f", 5.5 );
  t.CmpStr( s, "05.50" );

  s.clear(); Bodacious::sprintf(s,"%0*.*f", 5, 2, 5.5 );
  t.CmpStr( s, "05.50" );

  s.clear(); Bodacious::sprintf(s,"%0*.*f", 10, 4, 7.23 );
  t.CmpStr( s, "00007.2300" );

  s.clear(); Bodacious::sprintf(s,"%ls", L"haha" );
  t.CmpStr( s, "haha" );

  wchar_t widetest[10000];
  char narrowtest[10000];
  int l = sizeof( widetest ) / sizeof( wchar_t ) - 1;
  for( int i = 0; i < l; ++i ) {
    widetest[i] = L'A';
    narrowtest[i] = 'A';
  }
  widetest[l] = 0;
  narrowtest[l] = 0;

  s.clear(); Bodacious::sprintf(s,"%ls", widetest );
  t.CmpStr( s, narrowtest );

  s.clear(); Bodacious::sprintf(s,"%s", narrowtest );
  t.CmpStr( s, narrowtest );

  t.Verdict();
};
