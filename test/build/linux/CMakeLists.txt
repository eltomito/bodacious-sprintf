#
# This file is part of Bodacious Printf.
#
# Bodacious Printf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Bodacious Printf is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bodacious Printf.  If not, see <https://www.gnu.org/licenses/>.
#
cmake_minimum_required(VERSION 2.6)
project(BODACIOUS_PRINTF_TEST)

set(CMAKE_CXX_FLAGS "-std=c++11 -Wreturn-type ${CMAKE_CXX_FLAGS}")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 ${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG=1")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")

set(SUPER_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../..)
set(SRC ${SUPER_DIR}/src)
set(TEST_SRC ${SUPER_DIR}/test/src)

set(BUILD_TYPE_DIR release)
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
	set(BUILD_TYPE_DIR debug)
endif()

message(BUILD_TYPE_DIR: ${BUILD_TYPE_DIR})
message(CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE})

cmake_policy(SET CMP0015 NEW) #link directories are relative to CMAKE_CURRENT_SOURCE_DIR

include_directories(. ${SRC} ${TEST_SRC})

add_executable(test
							${TEST_SRC}/test_bodacious_printf.cpp
							${SRC}/bodacious_printf.cpp
							)
